import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = environment.apiUrl;
const API_KEY = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  currentArticle: any;
  constructor(private http: HttpClient) { 
    this.http = http;
  }

  getData(url) {
    //console.log(url);
    return this.http.get(`${API_URL}/${url}&apikey=${API_KEY}`);
  }
}
